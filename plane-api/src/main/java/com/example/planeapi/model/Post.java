package com.example.planeapi.model;

import lombok.Data;

@Data
public class Post {
    private Long id;
    private String description;
}
