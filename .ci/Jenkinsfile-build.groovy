pipeline {
    agent any

    options {
        skipDefaultCheckout(true)
    }

    tools {
        maven 'maven3_8_4'
        jdk 'jdk8u322'
    }

    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }

        stage('Build') {
            steps {
                echo "Building ${env.JOB_NAME}"
                sh 'mvn clean install'
            }
        }

        stage('Test') {
            steps {
                // Preparing Git
                sh "git branch -u origin/${env.BRANCH_NAME} ${env.BRANCH_NAME}"
                sh "git checkout ${env.BRANCH_NAME}"
                sh "git pull"
                sh "git config user.email \"Mariusz\""
                sh "git config user.name \"maniuchh@gmail.com\""
                sh 'git status'
                sh "mvn build-helper:parse-version versions:set -DnewVersion='\${parsedVersion.majorVersion}.\${parsedVersion.minorVersion}.\${parsedVersion.nextIncrementalVersion}' versions:commit"
                sh 'git add .'
                sh 'git commit -m "[JENKINS] Working.."'
                sh 'git push'
            }
        }
    }
}